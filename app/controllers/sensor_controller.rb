class SensorController < ApplicationController
  before_action :set_sensor, only: [:show]

  def show
    render json: @sensor
  end

  private
  def set_sensor
    @sensor = InputSensor.find_by_name(params[:id])
  end
end
