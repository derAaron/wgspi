require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module WgsBackend
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.generators.assets           = false
    config.generators.helper           = false
    config.web_console.whitelisted_ips = ['10.8.0.0/16', '192.168.0.0/16'] if !Rails.env.test?
    config.middleware.insert_before 0, Rack::Cors do
      allow do
        origins 'localhost:4200'
        resource '*',
                 headers: :any,
                 methods: %i(get post put patch delete options head)
      end
    end
  end
end
