class CreateSettings < ActiveRecord::Migration[5.0]
  def change
    create_table :settings do |t|
      t.string :key
      t.decimal :value, default: 0.0

      t.timestamps
    end
  end
end
