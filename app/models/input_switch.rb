class InputSwitch < ApplicationRecord
  enum state: [:off, :on, :auto]

  belongs_to :output_trigger,
             foreign_key: :name,
             primary_key: :name,
             optional:    true

  def self.for key
    find_or_create_by name: key
  end

  def self.initialize
    self.for('window').update_attributes(title: 'Fenster', icon: 'window')
    self.for('fan').update_attributes(title: 'Lüfter', icon: 'fan')
    self.for('heater').update_attributes(title: 'Heizung', icon: 'heater')
    self.for('dryer').update_attributes(title: 'Entfeuchter', icon: 'dryer')
    self.for('light').update_attributes(title: 'Licht', icon: 'light')
    self.for('door').update_attributes(title: 'Tür', icon: 'door')
    # self.for('special').update_attributes(title: 'Spezial', icon: 'special1')
  end
end
