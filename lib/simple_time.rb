class SimpleTime
  @hour = 0
  @min = 0

  attr_reader :hour, :min

  def initialize hour, minute
    if minute.between?(0, 59) && hour.between?(0, 23)
      @hour = hour
      @min = minute
    else
      raise 'not a time'
    end
  end

  def between? start, finish
    if start.value <= finish.value
      return start <= self && self <= finish
    else
      return !(finish <= self && self <= start)
    end
  end

  def self.now
    now = Time.now
    SimpleTime.new now.hour, now.min
  end

  def value
    @hour * 100 + @min
  end

  def to_s
    "#{"%02d" % @hour}:#{"%02d" % @min}"
  end

  def <=(other)
    if self.hour == other.hour
      self.min <= other.min
    else
      self.hour <= other.hour
    end
  end
end