Feature: Window

  Scenario: Window switch auto => Window opens and closes according to the temperature
    Given I set switch "window" to "auto"
    And the last output was
      | window | false |
    And the weather is
      | type           | name    | key         | value |
      | Sensor         | inside  | temperature | 18.0  |
      | Sensor         | window  | temperature | 18.0  |
      | Weatherstation | outside | temperature | 18.0  |
    And the settings are
      | window_min_inside_temperature  | 23 |
      | window_min_outside_temperature | 25 |

    When the core runs

    Then the output of "window" is set to "false"

    When the weather is
      | type   | name   | key         | value |
      | Sensor | inside | temperature | 23    |
      | Sensor | window | temperature | 23    |
    And the core runs

    Then the output of "window" is set to "false"

    When the weather is
      | type   | name   | key         | value |
      | Sensor | inside | temperature | 23.6  |
      | Sensor | window | temperature | 23.6  |
    And the core runs

    Then the output of "window" is set to "true"

    When the weather is
      | type   | name   | key         | value |
      | Sensor | inside | temperature | 22    |
      | Sensor | window | temperature | 24    |
    And the core runs

    Then the output of "window" is set to "true"

    When the weather is
      | type   | name   | key         | value |
      | Sensor | inside | temperature | 22.8  |
      | Sensor | window | temperature | 22    |
    And the core runs

    Then the output of "window" is set to "false"

    When the weather is
      | type           | name    | key         | value |
      | Weatherstation | outside | temperature | 25    |
    And the core runs

    Then the output of "window" is set to "false"

    When the weather is
      | type           | name    | key         | value |
      | Weatherstation | outside | temperature | 25.6  |
    And the core runs

    Then the output of "window" is set to "true"

    When the weather is
      | type           | name    | key         | value |
      | Weatherstation | outside | temperature | 25    |
    And the core runs

    Then the output of "window" is set to "true"

    When the weather is
      | type           | name    | key         | value |
      | Weatherstation | outside | temperature | 24.4  |
    And the core runs

    Then the output of "window" is set to "false"

  Scenario: Window switch off => Window closed
    Given I set switch "window" to "off"
    And the last output was
      | window | false |
    And the weather is
      | type           | name    | key         | value |
      | Sensor         | inside  | temperature | 28.0  |
      | Sensor         | window  | temperature | 28.0  |
      | Weatherstation | outside | temperature | 28.0  |
    And the settings are
      | window_min_inside_temperature  | 23 |
      | window_min_outside_temperature | 25 |
    And the core runs

    Then the output of "window" is set to "false"

  Scenario: Window switch on => Window open
    Given I set switch "window" to "on"
    And the last output was
      | window | false |
    And the weather is
      | type           | name    | key         | value |
      | Sensor         | inside  | temperature | 18.0  |
      | Sensor         | window  | temperature | 18.0  |
      | Weatherstation | outside | temperature | 18.0  |
    And the settings are
      | window_min_inside_temperature  | 23 |
      | window_min_outside_temperature | 25 |
    And the core runs

    Then the output of "window" is set to "true"
