InputSensor.delete_all
InputSensor.for('inside').update(title: 'Box')
InputSensor.for('insidemix').update(title: 'Innen')
InputSensor.for('window').update(title: 'Fenster')

InputSolarpanel.delete_all
InputSolarpanel.for('roof').update(
    title:    'Dach',
    url:      WGS::SOLARPANEL_URL,
    selector: WGS::SOLARPOWER_SELECTOR
)

InputSwitch.delete_all
InputSwitch.for('window').update(title: 'Fenster', icon: 'window')
InputSwitch.for('fan').update(title: 'Lüfter', icon: 'fan')
InputSwitch.for('heater').update(title: 'Heizung', icon: 'heater')
InputSwitch.for('dryer').update(title: 'Entfeuchter', icon: 'dryer')
InputSwitch.for('light').update(title: 'Licht', icon: 'light')
InputSwitch.for('door').update(title: 'Tür', icon: 'door')
# InputSwitch.for('special').update(title: 'Spezial', icon: 'special1')

InputWeatherstation.delete_all
InputWeatherstation.for('outside').update(title: 'Außen')

InputWindow.delete_all
InputWindow.for('window_switch_1').update(title: 'Terrasse')
InputWindow.for('window_switch_2').update(title: 'Beet')
InputWindow.for('window_switch_3').update(title: 'Teich')

OutputTrigger.delete_all
OutputTrigger.for('window').update state: false
OutputTrigger.for('fan').update state: false
OutputTrigger.for('heater').update state: false
OutputTrigger.for('dryer').update state: false
OutputTrigger.for('light').update state: false
OutputTrigger.for('door').update state: false
# OutputTrigger.for('special').update state: false

Setting.delete_all
Setting.for('window_min_inside_temperature').update value: 23.0
Setting.for('window_min_outside_temperature').update value: 18.0
Setting.for('heating_max_inside_temperature').update value: 8.0
Setting.for('dryer_min_solar_power').update value: 500.0
Setting.for('dryer_start_time').update value: 900.0
Setting.for('dryer_end_time').update value: 2300.0
