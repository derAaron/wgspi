require 'net/http'
require 'open-uri'

class InputSolarpanel < ApplicationRecord

  def self.for key
    find_or_create_by name: key
  end

  def get_power
    timeout(10) do
      document  = Nokogiri::HTML(open(url, http_basic_authentication: ['pvserver', 'pvwr']))
      self.power= document.at(selector).text.to_i
      save
    end
  rescue Exception => e
    Rails.logger.debug "HTTP-Access-Error:\n\t#{e.message}"

    # send error over the cable
    ActionCable.server.broadcast 'update_channel', {
        action:   "Error",
        snackbar: "Keine Verbindung zum Solarpanel",
        console:  e.message
    }
  end
end
