Feature: Fan

  Scenario: Fan switch auto => fan on
    Given I set switch "fan" to "auto"
    Given I set switch "window" to "off"

    When the core runs

    Then the output of "fan" is set to "false"

    Then I set switch "window" to "on"

    When the core runs

    Then the output of "fan" is set to "true"

  Scenario: Fan switch off => fan off
    Given I set switch "fan" to "off"
    Given I set switch "window" to "off"

    When the core runs

    Then the output of "fan" is set to "false"

    Then I set switch "window" to "on"

    When the core runs

    Then the output of "fan" is set to "false"

  Scenario: Fan switch on => fan on
    Given I set switch "fan" to "on"
    Given I set switch "window" to "off"

    When the core runs

    Then the output of "fan" is set to "true"

    Then I set switch "window" to "on"

    When the core runs

    Then the output of "fan" is set to "true"