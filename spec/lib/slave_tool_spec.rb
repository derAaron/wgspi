require 'rails_helper'
require 'slave_tool'

describe 'SlaveTool' do
  describe 'getFloat' do
    let(:slave_tool){SlaveTool.new 0x23}
    it 'doesn\'t raise exception if two threads are accessing simultaneously' do
      t = Thread.new do
        expect{slave_tool.getFloat}.to_not raise_exception Exception
      end
      expect{slave_tool.getFloat}.to_not raise_exception Exception

      t.join
    end
  end
end
