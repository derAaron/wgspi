require 'slave_getter'
require 'slave_setter'

class SlaveAdaptor
  def initialize
    @send_keys = [
        SlaveSetter.new(WGS::Fenster_auf,
                        OutputTrigger.for('window'),
                        true),
        SlaveSetter.new(WGS::Fenster_zu,
                        OutputTrigger.for('window'),
                        false),
        SlaveSetter.new(WGS::Heizung_an,
                        OutputTrigger.for('heater'),
                        true),
        SlaveSetter.new(WGS::Heizung_aus,
                        OutputTrigger.for('heater'),
                        false),
        SlaveSetter.new(WGS::Luefter_an,
                        OutputTrigger.for('fan'),
                        true),
        SlaveSetter.new(WGS::Luefter_aus,
                        OutputTrigger.for('fan'),
                        false),
        SlaveSetter.new(WGS::Licht_an,
                        OutputTrigger.for('light'),
                        true),
        SlaveSetter.new(WGS::Licht_aus,
                        OutputTrigger.for('light'),
                        false),
        SlaveSetter.new(WGS::Entfeuchter_an,
                        OutputTrigger.for('dryer'),
                        true),
        SlaveSetter.new(WGS::Entfeuchter_aus,
                        OutputTrigger.for('dryer'),
                        false),
        SlaveSetter.new(WGS::Tuer_auf,
                        OutputTrigger.for('door'),
                        true),
        SlaveSetter.new(WGS::Tuer_zu,
                        OutputTrigger.for('door'),
                        false)#,
        # SlaveSetter.new(WGS::Spezial_an,
        #                 OutputTrigger.for('special'),
        #                 true),
        # SlaveSetter.new(WGS::Spezial_aus,
        #                 OutputTrigger.for('special'),
        #                 false)
    ]

    @get_keys = [
        SlaveGetter.new(WGS::Temp_int,
                        InputSensor.for('inside'),
                        :temperature_raw),
        SlaveGetter.new(WGS::Hygro_int,
                        InputSensor.for('inside'),
                        :humidity_raw),
        SlaveGetter.new(WGS::Temp_fenster,
                        InputSensor.for('window'),
                        :temperature_raw),
        SlaveGetter.new(WGS::Hygro_fenster,
                        InputSensor.for('window'),
                        :humidity_raw),
        SlaveGetter.new(WGS::Temp_ext,
                        InputWeatherstation.for('outside'),
                        :temperature_raw),
        SlaveGetter.new(WGS::Hygro_ext,
                        InputWeatherstation.for('outside'),
                        :humidity_raw),
        SlaveGetter.new(WGS::Regenmenge_ext,
                        InputWeatherstation.for('outside'),
                        :rain_amount_raw),
        SlaveGetter.new(WGS::Regen_ext,
                        InputWeatherstation.for('outside'),
                        :raining),
        SlaveGetter.new(WGS::Wind_ext,
                        InputWeatherstation.for('outside'),
                        :wind_speed),
        SlaveGetter.new(WGS::Fenster_1_zu,
                        InputWindow.for('window_switch_1'),
                        :closed),
        SlaveGetter.new(WGS::Fenster_2_zu,
                        InputWindow.for('window_switch_2'),
                        :closed),
        SlaveGetter.new(WGS::Fenster_3_zu,
                        InputWindow.for('window_switch_3'),
                        :closed)
    ]
  end

  # Aufrufen wenn der Interupt am GPIO getriggert wird
  def readWeatherToDatabase
    @get_keys.each do |date|
      date.writeUpdateToDatabase
    end

    # Mittelwert für Insidemix berechnen
    InputSensor.for('insidemix').update_attributes \
      temperature: ((InputSensor.for('inside').temperature + InputSensor.for('window').temperature)/2).round(1),
      humidity:    ((InputSensor.for('inside').humidity + InputSensor.for('window').humidity)/2).round(1)
  end

  # Aufrufen wenn der Core mit run durch ist
  def sendTriggersToSlave
    #nach last update sortieren
    @send_keys.sort_by!{|key| key.get_row.input_switch.updated_at}.reverse!
    #jeden key senden
    @send_keys.each do |key|
      key.sendOutputToSlave
    end
  end
end
