require 'net/http'
require 'open-uri'

class Log
  URL         = "http://www.duimwap.de/wgs/"
  @@semaphore = Mutex.new

  def self.submit
    Thread.new do
      unless @@semaphore.locked?
        @@semaphore.synchronize do
          begin
            Timeout.timeout(10) do
              my_values = logstring
              Nokogiri::HTML(open("#{URL}?#{my_values};#{checksum(my_values)}"))
            end
          rescue Exception => e
            Rails.logger.debug "HTTP-Access-Error:\n\t#{e.message}"
          end
        end
      end
    end
  end

  def self.checksum string
    checksum = 0
    string.split('').each do |char|
      checksum = (checksum + char.ord) % 256
    end
    return checksum
  end

  def self.logstring
    values = [
        InputSensor.for('inside').temperature_raw,
        InputSensor.for('inside').humidity_raw,
        InputSensor.for('window').temperature_raw,
        InputSensor.for('window').humidity_raw,
        InputWeatherstation.for('outside').temperature_raw,
        InputWeatherstation.for('outside').humidity_raw,
        InputWeatherstation.for('outside').rain_amount_netto_raw,
        InputWeatherstation.for('outside').raining_raw,
        InputWeatherstation.for('outside').wind_speed,
        InputWindow.for('window_switch_1').closed_raw,
        InputWindow.for('window_switch_2').closed_raw,
        InputWindow.for('window_switch_3').closed_raw,
        InputSolarpanel.for('roof').power,
        OutputTrigger.for('window').state_raw,
        OutputTrigger.for('heater').state_raw,
        OutputTrigger.for('fan').state_raw,
        OutputTrigger.for('light').state_raw,
        OutputTrigger.for('dryer').state_raw
    ].join ','

    settings = [
        "#{WGS::Window_min_inside_temperature}:#{Setting.for('window_min_inside_temperature').value_raw}",
        "#{WGS::Window_min_outside_temperature}:#{Setting.for('window_min_outside_temperature').value_raw}",
        "#{WGS::Heating_max_inside_temperature}:#{Setting.for('heating_max_inside_temperature').value_raw}",
        "#{WGS::Dryer_min_solar_power}:#{Setting.for('dryer_min_solar_power').value}",
        "#{WGS::Dryer_start_time}:#{Setting.for('dryer_start_time').value}",
        "#{WGS::Dryer_end_time}:#{Setting.for('dryer_end_time').value}"
    ].join ','
    return "#{values},#{settings}"
  end
end