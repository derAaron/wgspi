class InputSensor < ApplicationRecord
  def self.for key
    find_or_create_by name: key
  end

  def self.initialize
    self.for('inside').update_attributes(title: 'Box')
    self.for('insidemix').update_attributes(title: 'Innen')
    self.for('window').update_attributes(title: 'Fenster')
  end
end
