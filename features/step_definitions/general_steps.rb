Then(/^reset the database$/) do
  DatabaseCleaner.clean
  DatabaseCleaner.start
  load Rails.root.join('db/seeds.rb')
end

Given(/^the time is "([^"]*)"$/) do |time|
  hour, min = time.split ':'
  Timecop.freeze(Time.new(1983,3,25,hour,min,0,Time.now.getlocal.strftime("%:z")))
end

When(/^the day is "([^"]*)"$/) do |date|
  Timecop.freeze(Time.strptime(date, "%d.%m.%Y"))
end
