class SolarpanelController < ApplicationController
  before_action :set_solarpanel, only: [:show]

  def show
    Thread.new do
      @solarpanel.get_power
    end
    render json: @solarpanel
  end

  private
  def set_solarpanel
    @solarpanel = InputSolarpanel.find_by_name(params[:id])
  end
end
