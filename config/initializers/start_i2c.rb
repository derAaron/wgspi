require 'slave_adaptor'
require 'core'
require 'log'

RASPI_PLATFORM = "armv6l-linux-eabihf"

if RUBY_PLATFORM == RASPI_PLATFORM
  require 'my_i2c'
  SLAVE = MyI2C.new 2
else
  require "i2c_mock"
  SLAVE = I2CMock.new
end
SLAVE_SEMAPHORE = Mutex.new

# Wait for Interrupt on GPIO-Pin 5
Thread.abort_on_exception = true
THREAD_INT                = Thread.new do

  if RUBY_PLATFORM == RASPI_PLATFORM
    require 'rpi_gpio'
  else
    require 'rpi_mock'
  end

  RPi::GPIO.set_numbering :bcm
  RPi::GPIO.setup 5, :as => :input
  interrupt = false
  loop do
    sleep 0.01
    new_interrupt = RPi::GPIO.high? 5
    if interrupt && !new_interrupt
      Core.resetRainOffsetAtMidnight
      SlaveAdaptor.new.readWeatherToDatabase
      InputSolarpanel.for('roof').get_power
      Core.run
      Log.submit #auf duimwap.de loggen
      Rails.logger.debug "Nano Interrupt"
    end
    interrupt = new_interrupt
  end
end
