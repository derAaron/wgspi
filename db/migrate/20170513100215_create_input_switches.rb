class CreateInputSwitches < ActiveRecord::Migration[5.0]
  def change
    create_table :input_switches do |t|
      t.string :name
      t.string :title
      t.string :icon
      t.integer :state, default: 0

      t.timestamps
    end
  end
end
