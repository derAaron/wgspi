class MyI2C
  def initialize addr, dev = "/dev/i2c-1"
    @addr = addr
    @dev  = dev
  end

  def i2cset *bytes
    loop do
      begin
        i2c = File.open(@dev, "r+")
        i2c.ioctl 0x0706, @addr
        i2c.syswrite bytes.pack("C*")
        i2c.close
        return
      rescue Exception => e
        sleep 0.05
      end
    end
  end

  def i2cget len=32
    loop do
      begin
        i2c = File.open(@dev, "r")
        i2c.ioctl 0x0706, @addr
        value = i2c.sysread len
        i2c.close
        return value
      rescue Exception => e
        sleep 0.05
      end
    end
  end
end