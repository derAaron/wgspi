class CreateInputSensors < ActiveRecord::Migration[5.0]
  def change
    create_table :input_sensors do |t|
      t.string :name
      t.string :title
      t.decimal :temperature, default: 0.0
      t.decimal :humidity, default: 0.0

      t.timestamps
    end
  end
end
