Given(/^the weather is$/) do |table|
  hashes = table.hashes
  hashes.each do |hash|
    record = "Input#{hash[:type].to_s.capitalize}".constantize \
                 .find_by_name(hash[:name].downcase)
    record.send "#{hash[:key].downcase}=", hash[:value]
    record.save
  end
end