require 'slave_tool'

class SlaveGetter < SlaveTool
  @value
  @save_row
  @save_field

  def initialize key, save_row = nil, save_field = nil
    super key
    @save_row   = save_row
    @save_field = save_field
  end

  def writeUpdateToDatabase
    @save_row.reload
    new_value = self.getFloat
    return if new_value == nil
    if (@save_row.send @save_field).in? [true, false]
      @save_row.send "#{@save_field}=", new_value != 0.0
    else
      @save_row.send "#{@save_field}=", new_value
    end
    @save_row.save
  end
end