class ApplicationRecord < ActiveRecord::Base
  before_update :notify_subscriber
  FACTOR = 10;

  self.abstract_class = true

  def notify_subscriber
    if changed? && !ActionCable.server.logger.nil?
      if respond_to? :name
        identifier = name
      elsif  respond_to? :key
        identifier = key
      end
      ActionCable.server.broadcast 'update_channel', as_json.merge(action: "#{self.class}.#{identifier}")
    end
  end

  # generic method creation for _raw methods
  [
      :temperature,
      :humidity,
      :rain_amount,
      :rain_amount_netto,
      :value
  ].each do |field|
    define_method "#{field}_raw" do
      raise NoMethodError if !respond_to? field
      value = send field
      return (value*FACTOR).to_i if value
    end

    define_method "#{field}_raw=" do |value|
      raise NoMethodError if !respond_to? field
      self.send "#{field}=", value.to_f / FACTOR
    end
  end
end
