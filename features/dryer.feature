Feature: Dryer

  Scenario: Dryer switch auto
    Given I set switch "dryer" to "auto"
    And the settings are
      | dryer_min_solar_power | 500  |
      | dryer_start_time      | 0500 |
      | dryer_end_time        | 1900 |
    And the weather is
      | type       | name | key   | value |
      | Solarpanel | roof | power | 1650  |
    And the time is "14:33"

    When the core runs

    Then the output of "dryer" is set to "true"

    Given the time is "19:01"

    When the core runs

    Then the output of "dryer" is set to "false"

    Given the time is "4:59"

    When the core runs

    Then the output of "dryer" is set to "false"

    Given the time is "5:00"

    When the core runs

    Then the output of "dryer" is set to "true"

  Scenario: Dryer switch auto with start end end switched
    Given I set switch "dryer" to "auto"
    And the settings are
      | dryer_min_solar_power | 500  |
      | dryer_start_time      | 1900 |
      | dryer_end_time        | 0500 |
    And the weather is
      | type       | name | key   | value |
      | Solarpanel | roof | power | 1650  |

    Given the time is "4:29"

    When the core runs

    Then the output of "dryer" is set to "true"

    Given the time is "4:49"

    When the core runs

    Then the output of "dryer" is set to "true"

    Given the time is "5:00"

    When the core runs

    Then the output of "dryer" is set to "false"

    Given the time is "14:33"

    When the core runs

    Then the output of "dryer" is set to "false"

    Given the time is "19:01"

    When the core runs

    Then the output of "dryer" is set to "true"


  Scenario: It switches on and off according to the limit
    Given I set switch "dryer" to "auto"
    And the settings are
      | dryer_min_solar_power | 500  |
      | dryer_start_time      | 0500 |
      | dryer_end_time        | 1900 |
    And the time is "06:00"

    Given the weather is
      | type       | name | key   | value |
      | Solarpanel | roof | power | 0     |

    When the core runs

    Then the output of "dryer" is set to "false"

    Given the weather is
      | type       | name | key   | value |
      | Solarpanel | roof | power | 500   |
    And the time is "07:00"

    When the core runs

    Then the output of "dryer" is set to "false"

    Given the weather is
      | type       | name | key   | value |
      | Solarpanel | roof | power | 601   |
    And the time is "08:00"

    When the core runs

    Then the output of "dryer" is set to "true"

    Given the weather is
      | type       | name | key   | value |
      | Solarpanel | roof | power | 6000  |
    And the time is "09:00"

    When the core runs

    Then the output of "dryer" is set to "true"

    Given the weather is
      | type       | name | key   | value |
      | Solarpanel | roof | power | 500   |
    And the time is "10:00"

    When the core runs

    Then the output of "dryer" is set to "true"

    Given the weather is
      | type       | name | key   | value |
      | Solarpanel | roof | power | 399   |
    And the time is "11:00"

    When the core runs

    Then the output of "dryer" is set to "false"

  Scenario: The dryer is not switched off before 30min passed since turning on
    Given I set switch "dryer" to "auto"
    And the settings are
      | dryer_min_solar_power | 500  |
      | dryer_start_time      | 0500 |
      | dryer_end_time        | 1900 |
    And the time is "06:00"

    Given the weather is
      | type       | name | key   | value |
      | Solarpanel | roof | power | 0     |

    When the core runs

    Then the output of "dryer" is set to "false"

    Given the weather is
      | type       | name | key   | value |
      | Solarpanel | roof | power | 700   |
    And the time is "07:00"

    When the core runs

    Then the output of "dryer" is set to "true"

    Given the weather is
      | type       | name | key   | value |
      | Solarpanel | roof | power | 300   |
    And the time is "07:30"

    When the core runs

    Then the output of "dryer" is set to "true"

    Given the weather is
      | type       | name | key   | value |
      | Solarpanel | roof | power | 300   |
    And the time is "07:31"

    When the core runs

    Then the output of "dryer" is set to "false"

  Scenario: Dryer switch off, on, off
    Given the time is "06:00"
    And I set switch "dryer" to "off"
    And the settings are
      | dryer_min_solar_power | 500  |
      | dryer_start_time      | 0500 |
      | dryer_end_time        | 1900 |
    And the weather is
      | type       | name | key   | value |
      | Solarpanel | roof | power | 1000  |

    When the core runs

    Then the output of "dryer" is set to "false"

    Given the time is "06:01"
    And I set switch "dryer" to "on"

    When the core runs

    Then the output of "dryer" is set to "true"

    Given the time is "06:02"
    And I set switch "dryer" to "off"

    When the core runs

    Then the output of "dryer" is set to "false"