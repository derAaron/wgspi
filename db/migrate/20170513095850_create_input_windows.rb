class CreateInputWindows < ActiveRecord::Migration[5.0]
  def change
    create_table :input_windows do |t|
      t.string :name
      t.string :title
      t.boolean :closed, default: false

      t.timestamps
    end
  end
end
