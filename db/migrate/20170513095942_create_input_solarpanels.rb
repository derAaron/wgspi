class CreateInputSolarpanels < ActiveRecord::Migration[5.0]
  def change
    create_table :input_solarpanels do |t|
      t.string :name
      t.string :title
      t.string :url
      t.string :selector
      t.integer :power, default: 0

      t.timestamps
    end
  end
end
