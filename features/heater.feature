Feature: Heater

  Scenario: Heater switch auto => Heater on
    Given I set switch "heater" to "auto"
    And the settings are
      | heating_max_inside_temperature | 8 |
    And the weather is
      | type   | name            | key         | value |
      | Sensor | inside          | temperature | 18.0  |
      | Sensor | window          | temperature | 18.0  |
      | Window | window_switch_1 | closed      | true  |
      | Window | window_switch_2 | closed      | true  |
      | Window | window_switch_3 | closed      | true  |

    When the core runs

    Then the output of "heater" is set to "false"

    Given the weather is
      | type   | name   | key         | value |
      | Sensor | inside | temperature | 8.0   |
      | Sensor | window | temperature | 8.0   |

    When the core runs

    Then the output of "heater" is set to "false"

    Given the weather is
      | type   | name   | key         | value |
      | Sensor | inside | temperature | 7.0   |
      | Sensor | window | temperature | 7.8   |

    When the core runs

    Then the output of "heater" is set to "true"

    Given the weather is
      | type   | name   | key         | value |
      | Sensor | inside | temperature | 8.0   |
      | Sensor | window | temperature | 8.0   |

    When the core runs

    Then the output of "heater" is set to "true"

    Given the weather is
      | type   | name            | key    | value |
      | Window | window_switch_1 | closed | true  |
      | Window | window_switch_2 | closed | false |
      | Window | window_switch_3 | closed | true  |

    When the core runs

    Then the output of "heater" is set to "false"

    Given the weather is
      | type   | name            | key    | value |
      | Window | window_switch_1 | closed | true  |
      | Window | window_switch_2 | closed | true  |
      | Window | window_switch_3 | closed | true  |

    When the core runs

    Then the output of "heater" is set to "false"

    Given the weather is
      | type   | name   | key         | value |
      | Sensor | inside | temperature | 6.0   |
      | Sensor | window | temperature | 7.0   |

    When the core runs

    Then the output of "heater" is set to "true"

    Given the weather is
      | type   | name            | key    | value |
      | Window | window_switch_1 | closed | false |
      | Window | window_switch_2 | closed | true  |
      | Window | window_switch_3 | closed | true  |

    When the core runs

    Then the output of "heater" is set to "false"

    Given the weather is
      | type   | name            | key    | value |
      | Window | window_switch_1 | closed | true  |
      | Window | window_switch_2 | closed | true  |
      | Window | window_switch_3 | closed | false |

    When the core runs

    Then the output of "heater" is set to "false"


  Scenario: Fan switch off => Heater off
    Given I set switch "heater" to "off"
    And the settings are
      | heating_max_inside_temperature | 8 |
    And the weather is
      | type   | name            | key         | value |
      | Sensor | inside          | temperature | 3.0   |
      | Sensor | window          | temperature | 4.0   |
      | Window | window_switch_1 | closed      | true  |
      | Window | window_switch_2 | closed      | true  |
      | Window | window_switch_3 | closed      | true  |

    When the core runs

    Then the output of "heater" is set to "false"

  Scenario: Fan switch on => Heater on
    Given I set switch "heater" to "on"
    And the settings are
      | heating_max_inside_temperature | 8 |
    And the weather is
      | type   | name            | key         | value |
      | Sensor | inside          | temperature | 13.0  |
      | Sensor | window          | temperature | 14.0  |
      | Window | window_switch_1 | closed      | false |
      | Window | window_switch_2 | closed      | false |
      | Window | window_switch_3 | closed      | false |

    When the core runs

    Then the output of "heater" is set to "true"