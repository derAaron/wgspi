require 'slave_tool'

class SlaveSetter < SlaveTool

  attr_accessor :get_row

  @get_row
  @get_value

  def initialize key, get_row = nil, get_value = true
    super key
    @get_row   = get_row
    @get_value = get_value
  end

  def sendOutputToSlave
    if @get_row.state == @get_value
      self.getFloat
    end
  end
end