require 'open-uri'
require 'net/http'
require 'json'

class I2CMock
  @@hardware_resource_in_use = false
  @command                   = 0;

  def initialize
    url   = URI.parse('http://duimwap.de/wgs/yunremote.php')
    req   = Net::HTTP::Get.new(url.to_s)
    res   = Net::HTTP.start(url.host, url.port) { |http|
      http.request(req)
    }
    @data = JSON.parse(res.body)
  end

  def i2cset *args
    block_hardware
    @command = args[1].chr
  end

  def i2cget b
    block_hardware
    return "{#{@command}:#{@data[@command.ord.to_s]}}"
  end

  def block_hardware
    raise(IOError.new("i2c in use")) if @@hardware_resource_in_use
    @@hardware_resource_in_use = true
    sleep 0.1
    @@hardware_resource_in_use = false
  end
end