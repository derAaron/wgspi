Given(/^the last output was$/) do |table|
  table.rows_hash.each do |key, value|
    OutputTrigger.for(key).update state: (value == 'true')
  end
end

Then(/^the output of "([^"]*)" is set to "([^"]*)"$/) do |trigger, state|
  expect(OutputTrigger.for(trigger).state).to eq(state == 'true')
end
