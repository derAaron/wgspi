class InputWindow < ApplicationRecord

  def self.initialize
    self.for('window_switch_1').update_attributes(title: 'Terrasse')
    self.for('window_switch_2').update_attributes(title: 'Beet')
    self.for('window_switch_3').update_attributes(title: 'Teich')
  end

  def self.for key
    find_or_create_by name: key
  end

  def closed_raw
    closed ? 1 : 0
  end
end
