require 'simple_time'

class Setting < ApplicationRecord

  def self.for key
    find_or_create_by key: key
  end

  def to_simple_time
    SimpleTime.new (value/100).to_i, (value % 100)
  end
end
