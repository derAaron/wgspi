require 'slave_adaptor'
require 'core'

class ArduinoController < ApplicationController
  def show
    # FIXME muss wieder raus
    # SlaveAdaptor.new.readWeatherToDatabase
    # InputSolarpanel.for('roof').update_attributes power: Random.rand(6000)

    render json: {
        WGS::Temp_int                       => InputSensor.for('inside').temperature_raw,
        WGS::Hygro_int                      => InputSensor.for('inside').humidity_raw,
        WGS::Temp_fenster                   => InputSensor.for('window').temperature_raw,
        WGS::Hygro_fenster                  => InputSensor.for('window').humidity_raw,
        WGS::Temp_ext                       => InputWeatherstation.for('outside').temperature_raw,
        WGS::Hygro_ext                      => InputWeatherstation.for('outside').humidity_raw,
        WGS::Regenmenge_ext                 => InputWeatherstation.for('outside').rain_amount_netto_raw,
        WGS::Regen_ext                      => InputWeatherstation.for('outside').raining_raw,
        WGS::Wind_ext                       => InputWeatherstation.for('outside').wind_speed,
        WGS::Solarleistung                  => InputSolarpanel.for('roof').power,
        WGS::Fenster_1_zu                   => InputWindow.for('window_switch_1').closed_raw,
        WGS::Fenster_2_zu                   => InputWindow.for('window_switch_2').closed_raw,
        WGS::Fenster_3_zu                   => InputWindow.for('window_switch_3').closed_raw,

        WGS::Window_min_inside_temperature  => Setting.for('window_min_inside_temperature').value_raw,
        WGS::Window_min_outside_temperature => Setting.for('window_min_outside_temperature').value_raw,
        WGS::Heating_max_inside_temperature => Setting.for('heating_max_inside_temperature').value_raw,
        WGS::Dryer_min_solar_power          => Setting.for('dryer_min_solar_power').value,
        WGS::Dryer_start_time               => Setting.for('dryer_start_time').value,
        WGS::Dryer_end_time                 => Setting.for('dryer_end_time').value,

        WGS::DachfensterOffen               => OutputTrigger.for('window').state_raw,
        WGS::LuefterAn                      => OutputTrigger.for('fan').state_raw,
        WGS::HeizungAn                      => OutputTrigger.for('heater').state_raw,
        WGS::EntfeuchterAn                  => OutputTrigger.for('dryer').state_raw,
        WGS::LichtAn                        => OutputTrigger.for('light').state_raw,

        WGS::Last_update                    => [
            InputSensor.maximum(:updated_at),
            InputWeatherstation.maximum(:updated_at),
            InputWindow.maximum(:updated_at),
            InputSolarpanel.maximum(:updated_at),
        ].max.getlocal.strftime("%H:%M:%S")
    }
  end

  def update
    # falls es sich um Settings handelt
    const   = (WGS.constants.find { |k| WGS.const_get(k).to_s == params[:key] })
    setting = Setting.for const.to_s.downcase if const
    if setting
      if params[:key].to_i == WGS::Dryer_start_time || params[:key].to_i == WGS::Dryer_end_time || params[:key].to_i == WGS::Dryer_min_solar_power
        setting.value= params[:value]
      else
        setting.value_raw= params[:value]
      end
      setting.save
    elsif params[:key] == 'APP'
      find_and_trigger_switch_for_code params[:value].to_i
    end

    Core.run

    redirect_to action: "show"
  end

  private
  def find_and_trigger_switch_for_code code
    switches = [
        InputSwitch.for('window'),
        InputSwitch.for('fan'),
        InputSwitch.for('heater'),
        InputSwitch.for('dryer'),
        InputSwitch.for('light')
    ]
    sw       = switches[code/10]
    sw.state = code % 10
    sw.save
  end
end