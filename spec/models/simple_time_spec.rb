require 'rails_helper'

describe 'SimpleTime' do
  describe '#new' do
    it 'sets the value to 0 for 00:00' do
      expect(SimpleTime.new(0, 0).value).to eq 0
    end

    it 'sets the value to 2359 for 23:59' do
      expect(SimpleTime.new(23, 59).value).to eq 2359
    end

    it 'sets the value to 1234 for 12:34' do
      expect(SimpleTime.new(12, 34).value).to eq 1234
    end

    it 'raises an exception for -3:-46' do
      expect { SimpleTime.new(-3, -46).value }.to raise_error 'not a time'
    end

    it 'raises an exception for 24:60' do
      expect { SimpleTime.new(-3, -46).value }.to raise_error 'not a time'
    end

    it 'raises an exception for 24:45' do
      expect { SimpleTime.new(-3, -46).value }.to raise_error 'not a time'
    end

    it 'raises an exception for 13:60' do
      expect { SimpleTime.new(-3, -46).value }.to raise_error 'not a time'
    end
  end

  describe '#<=' do
    it 'works as expected' do
      expect(SimpleTime.new(0, 0) <= SimpleTime.new(23, 59)).to eq true
    end

    it 'works as expected' do
      expect(SimpleTime.new(12, 22) <= SimpleTime.new(12, 23)).to eq true
    end

    it 'works as expected' do
      expect(SimpleTime.new(12, 52) <= SimpleTime.new(13, 22)).to eq true
    end

    it 'works as expected' do
      expect(SimpleTime.new(12, 22) <= SimpleTime.new(12, 22)).to eq true
    end

    it 'works as expected' do
      expect(SimpleTime.new(23, 59) <= SimpleTime.new(0, 0)).to eq false
    end

    it 'works as expected' do
      expect(SimpleTime.new(12, 23) <= SimpleTime.new(12, 22)).to eq false
    end

    it 'works as expected' do
      expect(SimpleTime.new(13, 22) <= SimpleTime.new(12, 52)).to eq false
    end
  end

  describe '#between?' do
    let(:first) { SimpleTime.new(9, 00) }
    let(:later) { SimpleTime.new(12, 13) }
    let(:last) { SimpleTime.new(19, 59) }

    it 'returns true for first -> later -> last' do
      expect(later.between? first, last).to eq true
    end

    it 'returns true for later -> last -> first' do
      expect(last.between? later, first).to eq true
    end

    it 'returns true for last -> first -> later' do
      expect(first.between? last, later).to eq true
    end

    it 'returns false for last -> later -> first' do
      expect(later.between? last, first).to eq false
    end

    it 'returns false for first -> last -> later' do
      expect(last.between? first, later).to eq false
    end

    it 'returns false for later -> first -> last' do
      expect(first.between? later, last).to eq false
    end
  end

  describe '#now' do
    before do
      allow(Time).to receive(:now).and_return(Time.new(1983, 3, 25, 5, 49, 0))
    end

    it 'returns 549 as value' do
      expect(SimpleTime.now.value).to eq 549
    end
  end

  describe '#to_s' do
    it 'returns 00:00 for midnight' do
      expect(SimpleTime.new(0,0).to_s).to eq '00:00'
    end

    it 'returns 23:59 for a minute before midnight' do
      expect(SimpleTime.new(23,59).to_s).to eq '23:59'
    end
  end
end