require 'slave_adaptor'
require 'simple_time'

class Core
  def self.run
    # reset InputSwitches that have not been set on the same day except for fan
    InputSwitch.all.each do |switch|
    # InputSwitch.where.not(name: 'fan').each do |switch|
      if switch.updated_at.getlocal.to_date != Time.now.to_date
        switch.update state: :auto
      end
    end

    # calculate median temp
    inside_median_temp = (InputSensor.for('inside').temperature +
        InputSensor.for('window').temperature) / 2

    # Dachfenster
    OutputTrigger.for('window').autoSet true do
      inside_median_temp >
          Setting.for('window_min_inside_temperature').value.to_f + WGS::TEMP_HYSTERESIS \
      ||
          InputWeatherstation.for('outside').temperature >
              Setting.for('window_min_outside_temperature').value + WGS::TEMP_HYSTERESIS
    end

    OutputTrigger.for('window').autoSet false do
      inside_median_temp <
          Setting.for('window_min_inside_temperature').value - WGS::TEMP_HYSTERESIS \
      &&
          InputWeatherstation.for('outside').temperature <
              Setting.for('window_min_outside_temperature').value - WGS::TEMP_HYSTERESIS
    end

    # Lüfter
    OutputTrigger.for('fan').autoSet true do
      OutputTrigger.for('window').state
    end

    OutputTrigger.for('fan').autoSet false do
      !OutputTrigger.for('window').state
    end

    # Fußbodenheizung
    OutputTrigger.for('heater').autoSet true do
      inside_median_temp <
          Setting.for('heating_max_inside_temperature').value - WGS::TEMP_HYSTERESIS \
      && (
      InputWindow.for('window_switch_1').closed &&
          InputWindow.for('window_switch_2').closed &&
          InputWindow.for('window_switch_3').closed
      )
    end

    OutputTrigger.for('heater').autoSet false do
      inside_median_temp >
          Setting.for('heating_max_inside_temperature').value + WGS::TEMP_HYSTERESIS \
      || !(
      InputWindow.for('window_switch_1').closed &&
          InputWindow.for('window_switch_2').closed &&
          InputWindow.for('window_switch_3').closed
      )
    end

    # Entfeuchter
    start_time = Setting.for('dryer_start_time').to_simple_time
    end_time   = Setting.for('dryer_end_time').to_simple_time

    OutputTrigger.for('dryer').autoSet true do
      InputSolarpanel.for('roof').power >
          Setting.for('dryer_min_solar_power').value + WGS::POWER_HYSTERESIS \
      &&
          SimpleTime.now.between?(
              start_time, end_time)
    end

    OutputTrigger.for('dryer').autoSet false do
      OutputTrigger.for('dryer').updated_at.getlocal < 30.minutes.ago.getlocal && (
      InputSolarpanel.for('roof').power <
          Setting.for('dryer_min_solar_power').value - WGS::POWER_HYSTERESIS \
      ||
          SimpleTime.now.between?(
              end_time, start_time)
      )
    end

    # Licht
    OutputTrigger.for('light').autoSet(true) { false }
    OutputTrigger.for('light').autoSet(false) { false }

    # Spezial 1
    OutputTrigger.for('door').autoSet(true) do
      inside_median_temp >
          Setting.for('window_min_inside_temperature').value.to_f + (2 * WGS::TEMP_HYSTERESIS) + WGS::DOOR_TEMP_OFFSET\
      ||
          InputWeatherstation.for('outside').temperature >
              Setting.for('window_min_outside_temperature').value + (2 * WGS::TEMP_HYSTERESIS) + WGS::DOOR_TEMP_OFFSET
    end
    OutputTrigger.for('door').autoSet(false) do
      inside_median_temp <
          Setting.for('window_min_inside_temperature').value - (2 * WGS::TEMP_HYSTERESIS) + WGS::DOOR_TEMP_OFFSET\
      &&
          InputWeatherstation.for('outside').temperature <
              Setting.for('window_min_outside_temperature').value - (2 * WGS::TEMP_HYSTERESIS) + WGS::DOOR_TEMP_OFFSET
    end

    # Spezial 2
    # OutputTrigger.for('special').autoSet(true) { false }
    # OutputTrigger.for('special').autoSet(false) { false }

    # Schaltbefehle senden
    # start sending in a Thread to not delay the execution
    Thread.new do
      SlaveAdaptor.new.sendTriggersToSlave
    end
  end

  def self.resetRainOffsetAtMidnight
    # reset the offset value at midnight
    iwsOutside = InputWeatherstation.for('outside')
    if iwsOutside.updated_at.getlocal.to_date != Time.now.to_date
      iwsOutside.update rain_amount_offset: iwsOutside.rain_amount
    end
  end
end
