Rails.application.routes.draw do
  get 'buttons/', to: 'buttons#show'
  get 'buttons/click/:name/:state', to: 'buttons#click'
  get 'buttons/last_update'

  resources :switch, only: [:index, :show, :update]
  resources :sensor, only: [:show]
  resources :solarpanel, only: [:show]
  resources :weatherstation, only: [:show]
  resources :window, only: [:index, :show]
  resources :system, only: [:index]
  get 'system/shutdown', to: 'system#shutdown'
  get 'system/wgsrestart', to: 'system#wgsrestart'

  get 'arduino/:', to: 'arduino#show'
  get 'arduino/:key::value', to: 'arduino#update'
end
