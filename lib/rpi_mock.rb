module RPi
  class GPIO
    @@state = false

    def self.set_numbering *args
    end

    def self.setup *args
    end

    def self.high? pin
      @@state
    end

    def self.low? pin
      !@@state
    end

    def self.trigger
      Thread.new do
        @@state = true
        sleep 0.05
        @@state = false
      end
    end
  end
end