class InputWeatherstation < ApplicationRecord
  FACTOR = 10;

  def self.initialize
    self.for('outside').update_attributes(title: 'Außen')
  end

  def self.for key
    find_or_create_by name: key
  end

  def raining_raw
    self.raining ? 1 : 0
  end

  def rain_amount_netto
    rain_amount - rain_amount_offset
  end
end
