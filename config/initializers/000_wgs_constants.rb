module WGS
  TEMP_HYSTERESIS = 0.5 #°C
  POWER_HYSTERESIS = 100 #W
  DOOR_TEMP_OFFSET = 2 #°C

  #SOLARPANEL_URL = 'http://localhost/pvwr.html'
  SOLARPANEL_URL = 'http://192.168.2.194'
  SOLARPOWER_SELECTOR = 'body > form > font > table:nth-child(2)'\
                        ' > tr:nth-child(4) > td:nth-child(3)'

  Last_update = 0x00

  # Schalter
  Fenster_zu = 0x30
  Fenster_auf = 0x31
  Heizung_an = 0x32
  Heizung_aus = 0x33
  Luefter_an = 0x34
  Luefter_aus = 0x35
  Licht_an = 0x36
  Licht_aus = 0x37
  Entfeuchter_an = 0x38
  Entfeuchter_aus = 0x39
  Tuer_auf = 0x3A
  Tuer_zu = 0x3B
  Spezial_an = 0x3C
  Spezial_aus = 0x3D

  # Wetterdaten
  Temp_int = 0x21
  Hygro_int = 0x22
  Temp_fenster = 0x23
  Hygro_fenster = 0x24
  Temp_ext = 0x25
  Hygro_ext = 0x26
  Regenmenge_ext = 0x27
  Regen_ext = 0x28
  Wind_ext = 0x29
  Fenster_1_zu = 0x2A
  Fenster_2_zu = 0x2B
  Fenster_3_zu = 0x2C
  Solarleistung = 0x41
  I2C_wait = 0x2F

  # Schwelenwerte
  Window_min_inside_temperature = 0x43
  Window_min_outside_temperature = 0x44
  Heating_max_inside_temperature = 0x45
  Dryer_min_solar_power = 0x46
  Dryer_start_time = 0x47
  Dryer_end_time = 0x48

  #Outputzustände
  DachfensterOffen = 0x51
  HeizungAn = 0x52
  LuefterAn = 0x53
  LichtAn = 0x54
  EntfeuchterAn = 0x55

end
