class WeatherstationController < ApplicationController
  before_action :set_weatherstation, only: [:show]

  def show
    render json: @weatherstation
  end

  private
  def set_weatherstation
    @weatherstation = InputWeatherstation.find_by_name(params[:id])
  end
end
