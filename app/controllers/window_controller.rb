class WindowController < ApplicationController
  before_action :set_window, only: [:show]

  def index
    render json: InputWindow.all
  end

  def show
    render json: @window
  end

  private
  def set_window
    @window = InputWindow.find_by_name(params[:id])
  end
end
