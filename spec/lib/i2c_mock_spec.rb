require 'i2c_mock'

describe 'I2cMock' do
  describe 'i2cset' do
    let(:slave) { I2CMock.new }
    it 'raises no Exception when it is accessed directly after each other' do
      expect do
        slave.i2cset(1, 2)
        slave.i2cset(3, 4)
      end.to_not raise_exception Exception
    end

    it 'raises Exception when two Threads are accessing it simultaneously' do
      t = Thread.new do
        expect{slave.i2cset(3, 4)}.to raise_exception IOError
      end
      slave.i2cset(1, 2)

      t.join
    end

    it 'raises no Exception when I wait a little bit' do
      expect do
        slave.i2cset(1, 2)
        sleep 0.05
        slave.i2cset(3, 4)
      end.to_not raise_exception IOError
    end
  end
end
