Given(/^I set switch "([^"]*)" to "([^"]*)"$/) do |switch, state|
  InputSwitch.for(switch).update state: state.to_sym
  InputSwitch.for(switch).touch
end

Then(/^switch "([^"]*)" is set to "([^"]*)"$/) do |switch, state|
  expect(InputSwitch.for(switch).state).to eq state
end