class CreateOutputTriggers < ActiveRecord::Migration[5.0]
  def change
    create_table :output_triggers do |t|
      t.string :name
      t.boolean :state, default: false

      t.timestamps
    end
  end
end
