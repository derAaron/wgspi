require 'rails_helper'
require 'rake'

describe 'SlaveAdaptor' do
  before do
    Rake::Task['db:seed'].invoke
  end

  describe '#readWeatherToDatabase' do
    before do
      Object.send(:remove_const, :SLAVE)
      SLAVE = I2CMock.new
      allow(Net::HTTP).to receive(:start).and_return \
      '{"0" : "22:28:29",
        "33" : "258",
        "34" : "316",
        "35" : "243",
        "36" : "410",
        "37" : "144",
        "38" : "700",
        "39" : "0",
        "40" : "0",
        "41" : "0",
        "42" : "1",
        "43" : "1",
        "44" : "1",
        "65" : "0",
        "81" : "1",
        "82" : "0",
        "83" : "0",
        "84" : "0",
        "85" : "0",
        "67" : "250",
        "68" : "220",
        "69" : "80",
        "70" : "500",
        "71" : "700",
        "72" : "2300" }'
    end

    let(:slave_adaptor) { SlaveAdaptor.new }

    it('saves the correct values to the corresponding fields') do
      slave_adaptor.readWeatherToDatabase
      puts InputSensor.all.inspect
      expect(false).to eq true
    end
  end
end