Feature: Reset of the switch states to auto after midnight

  Scenario: All switches get set to auto when they were not set on the same day

    Given the day is "25.03.1983"
    And I set switch "window" to "on"
    And I set switch "fan" to "off"
    And I set switch "heater" to "on"
    And I set switch "dryer" to "off"
    And I set switch "light" to "on"
    And I set switch "door" to "off"
#    And I set switch "special" to "on"

    When the day is "26.03.1983"
    And the core runs

    Then switch "window" is set to "auto"
    And switch "fan" is set to "auto"
    And switch "heater" is set to "auto"
    And switch "dryer" is set to "auto"
    And switch "light" is set to "auto"
    And switch "door" is set to "auto"
#    And switch "special" is set to "auto"

  Scenario: All switches stay the same on the same day

    Given the day is "25.03.1983"
    And I set switch "window" to "off"
    And I set switch "fan" to "on"
    And I set switch "heater" to "off"
    And I set switch "dryer" to "on"
    And I set switch "light" to "off"
    And I set switch "door" to "on"
#    And I set switch "special" to "off"

    And the core runs

    Then switch "window" is set to "off"
    And switch "fan" is set to "on"
    And switch "heater" is set to "off"
    And switch "dryer" is set to "on"
    And switch "light" is set to "off"
    And switch "door" is set to "on"
#    And switch "special" is set to "off"
