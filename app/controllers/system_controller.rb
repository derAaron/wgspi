class SystemController < ApplicationController

  def index
    if RUBY_PLATFORM == RASPI_PLATFORM
      render json: {
          cpu:  {
              value: `grep 'cpu ' /proc/stat | awk '{usage=($2+$4)*100/($2+$4+$5)} END {print usage}'`.to_i,
              max:   100
          },
          mem:  {
              value: `free | grep Mem | awk '{print $3}'`.to_i,
              max:   `free | grep Mem | awk '{print $2}'`.to_i
          },
          wifi: {
              value: `cat /proc/net/wireless | grep wlan0 | awk '{ print $3 }'`.to_i,
              max:   100,
              ssid:  `iwgetid -r`
          },
          ip:   {
              local:  `ifconfig wlan0 | grep 'inet ' | awk -F"[a-zA-Z: ',]+" '{print $2}'`.strip,
              remote: `ifconfig tun0 | grep 'inet ' | awk -F"[a-zA-Z: ',]+" '{print $2}'`.strip
          },
          time: {
              update: [
                          InputSensor.maximum(:updated_at),
                          InputWeatherstation.maximum(:updated_at),
                          InputWindow.maximum(:updated_at),
                          InputSolarpanel.maximum(:updated_at),
                      ].max,
              up:     `uptime -p | cut -d' ' -f 2,3,4,5`
          }
      }
    else
      render json: {
          cpu:  { value: 33, max: 100 },
          mem:  { value: 56453, max: 156465 },
          wifi: { value: 88, max: 100, ssid: "Heinz" },
          ip:   { local: "192.168.0.123", remote: "10.8.0.123" },
          time: {
              update: 'Fri, 11 Aug 2017 20:21:02 UTC +00:00',
              up:     "1 day, 20 hours, 50 minutes"
          }
      }
    end
  end

  def shutdown
    `shutdown` if RUBY_PLATFORM == RASPI_PLATFORM
    render json: 'shuting down'
  end

  def wgsrestart
    `service wgs-server restart` if RUBY_PLATFORM == RASPI_PLATFORM
    render json: 'restarting'
  end
end
