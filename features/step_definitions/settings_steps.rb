Given(/^the settings are$/) do |table|
  table.rows_hash.each do |key, value|
    Setting.for(key).update value: value
  end
end
