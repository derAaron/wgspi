require 'core'

class SwitchController < ApplicationController
  before_action :set_switch, only: [:show, :update]

  def index
    all = InputSwitch.all.map do |e|
      {
          input: e,
          output: OutputTrigger.for(e.name)
      }
    end
    render json: all
  end

  def show
    render json: { input: @switch, output: OutputTrigger.for(@switch.name)}
  end

  def update
    @switch.update_attributes state: params[:state].to_sym
    Core.run
    show
  end

  private
  def set_switch
    @switch = InputSwitch.for(params[:id])
  end
end
