class OutputTrigger < ApplicationRecord

  belongs_to :input_switch,
             foreign_key: :name,
             primary_key: :name,
             optional:    true

  def self.for key
    find_or_create_by name: key
  end

  def self.last_update
    maximum(:updated_at).to_s
  end

  def autoSet new_state
    if InputSwitch.for(self.name).off?
      self.state = false;
    elsif InputSwitch.for(self.name).on?
      self.state = true;
    elsif yield # triggers according to evaluation of the block
      self.state = new_state
    end
    save
  end

  def state_raw
    state ? "1": "0"
  end
end
