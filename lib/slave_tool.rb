class SlaveTool
  MAX_UPDATE_TIME     = 20
  SLAVE_WAIT_TIME     = 0.05
  SLAVE_RESPONSE_TIME = 0.002
  @name
  @key

  def initialize key
    @name = (WGS.constants.find { |k| WGS.const_get(k) == key })
    @key  = key
  end

  def getFloat
    start_time = Time.now
    Rails.logger.debug "I2C:\nI2C:\nI2C: ### Begin ###"
    loop do
      received        = nil

      # Semaphore to handle concurrent I2C requests
      start_wait_time = Time.now
      while SLAVE_SEMAPHORE.locked?
        Rails.logger.debug '.'
        Rails.logger.debug "Thread #{Thread.current.object_id} waited "\
                           "#{Time.now - start_wait_time}s for I2C!"\
                            unless SLAVE_SEMAPHORE.locked?
      end
      SLAVE_SEMAPHORE.synchronize do
        #I2C request
        SLAVE.i2cset("{".ord, @key, "}".ord)
        sleep SLAVE_RESPONSE_TIME
        received = SLAVE.i2cget 32
      end

      if received && received[0..2] == "{#{@key.chr}:" && received.length > 4 # I'ts a MATCH!!!

        Rails.logger.debug "I2C: {#{@key.chr}}"
        Rails.logger.debug "I2C: #{received}"
        Rails.logger.debug "I2C: took #{((Time.now - start_time) * 1000).to_i}ms"

        return received.scan(/:([^:^}].*)}/).first.last.to_f
      elsif start_time.to_i + MAX_UPDATE_TIME < Time.now.to_i
        Rails.logger.debug "I2C: Timeout after #{MAX_UPDATE_TIME} seconds"
        # send error over the cable
        ActionCable.server.broadcast 'update_channel', {
            action:   "Error",
            snackbar: "Fehler beim Empfang der Wetterdaten",
            console:  "Maximale Anzahl an Wiederholungen erreicht"
        }
        return nil
      end

      sleep SLAVE_WAIT_TIME
    end
  end

  def getName
    @name
  end
end