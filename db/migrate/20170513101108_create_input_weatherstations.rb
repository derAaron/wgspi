class CreateInputWeatherstations < ActiveRecord::Migration[5.0]
  def change
    create_table :input_weatherstations do |t|
      t.string :name
      t.string :title
      t.decimal :temperature, default: 0.0
      t.decimal :humidity, default: 0.0
      t.integer :wind_speed, default: 0
      t.decimal :rain_amount, default: 0.0
      t.decimal :rain_amount_offset, default: 0.0
      t.boolean :raining, default: false

      t.timestamps
    end
  end
end
