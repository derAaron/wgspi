#!/bin/sh
### BEGIN INIT INFO
# Provides:          WGS-Server
# Required-Start:
# Required-Stop:
# Default-Start:     5
# Default-Stop:      0 1 6
# Short-Description: Starts & Stops My Programm
# Description:       Starts & Stops My Programm
### END INIT INFO

PATH="/home/pi/.rvm/rubies/ruby-2.4.0/bin:$PATH"

#Switch case fuer den ersten Parameter
case "$1" in
    start)
 #Aktion wenn start uebergeben wird
        echo "Starting WGS-Server"
        cd /home/pi/wgspi
        rm -f tmp/pids/server.pid
        rails s -b 0.0.0.0 &
        ;;

    stop)
 #Aktion wenn stop uebergeben wird
        echo "Stoppe WGS-Server"
        cd /home/pi/wgspi
        kill -9 $(cat tmp/pids/server.pid)
        ;;

    restart)
 #Aktion wenn restart uebergeben wird
        echo "Restarte WGS-Server"
        cd /home/pi/wgspi
        kill -9 $(cat tmp/pids/server.pid)
        rails s -b 0.0.0.0 &
        ;;
 *)
 #Standard Aktion wenn start|stop|restart nicht passen
 echo "(start|stop|restart)"
 ;;
esac

exit 0
